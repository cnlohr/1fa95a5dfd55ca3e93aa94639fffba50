#include <stdio.h>
#include <sys/ioctl.h> //ioctl() call defenitions
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>

/* ESP8266 / ESP32 Dual-Transistor Boot Bridge:
 DTR  RTS  RST  GP0
  1    1    1    1
  0    0    1    1
  1    0    0    1
  0    1    1    0
	NOTE:  S(et) means 0, C(lear) means 1.
*/ 

int main( int argc, char ** argv )
{
	int fd;
	if( argc < 3 )
	{
		fprintf( stderr, "Error: ESP Boot Moder usage: %s [TTY Device] [1 for boot-mode, 0 for run-mode]\n", argv[0] );
		return -1;
	}
	fd = open( argv[1], O_RDWR | O_NOCTTY );

	int RTS_flag, DTR_flag;
	RTS_flag = TIOCM_RTS;
	DTR_flag = TIOCM_DTR;

	if( atoi( argv[2] ) )
	{
		ioctl(fd,TIOCMBIS,&RTS_flag);
		ioctl(fd,TIOCMBIC,&DTR_flag);
		usleep(20000);
		// Go through boot mode
		ioctl(fd,TIOCMBIS,&DTR_flag);
		ioctl(fd,TIOCMBIC,&RTS_flag);
		usleep(20000);
		ioctl(fd,TIOCMBIC,&DTR_flag);
		ioctl(fd,TIOCMBIC,&RTS_flag);
	}
	else
	{
		ioctl(fd,TIOCMBIS,&RTS_flag);
		ioctl(fd,TIOCMBIC,&DTR_flag);
		// Avoid boot mode (start right up)
		usleep(30000);
		ioctl(fd,TIOCMBIC,&DTR_flag);
		ioctl(fd,TIOCMBIC,&RTS_flag);
	}

	close(fd);
}